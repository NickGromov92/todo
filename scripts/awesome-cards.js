'use strict';

function Card(config) {
	var id = null;

	var cardTitle,
		cardText,
		cardImportant,
		cardBlock,
		titleEl,
		textEl,
		importantEl,
		deleteButtonEl,
		cardsBlock,
		changeTitleEl,
		changeTextEl,
		changeButtonEl;

	function createElements() {
		cardBlock = document.createElement('div');
		titleEl = document.createElement('h2');
		textEl = document.createElement('p');
		importantEl = document.createElement('input');
		deleteButtonEl = document.createElement('button');
		changeButtonEl = document.createElement('button');

		changeTitleEl = document.createElement('input');
		changeTextEl = document.createElement('input');
	}

	function addElements() {
		cardBlock.appendChild(titleEl);
		cardBlock.appendChild(changeTitleEl);
		cardBlock.appendChild(textEl);
		cardBlock.appendChild(changeTextEl);
		cardBlock.appendChild(importantEl);
		cardBlock.appendChild(changeButtonEl);
		cardBlock.appendChild(deleteButtonEl);

		cardsBlock.appendChild(cardBlock);
	}

	function addElementsContent() {
		cardBlock.classList.add('card-block');
		titleEl.textContent = cardTitle;
		textEl.textContent = cardText;
		deleteButtonEl.textContent = 'DELETE';
		changeButtonEl.textContent = 'CHANGE';
		changeButtonEl.setAttribute('data-action', 'change');

		importantEl.setAttribute('type', 'checkbox');
		if(cardImportant) {
			importantEl.setAttribute('checked', 'checked');
		}

		changeTitleEl.style.display = 'none';
		changeTextEl.style.display = 'none';
	}

	function generateCard () {
		createElements();
		addElementsContent();
		addElements();
		saveState();
	}

	function saveState() {
		if(!config.id) {
			generateId();
			serverData[id] = {
				title: cardTitle,
				text: cardText,
				important: cardImportant
			};
		} else {
			id = config.id;
		}
	}

	function updateState() {
		serverData[id].title = cardTitle;
		serverData[id].text = cardText;
		serverData[id].important = cardImportant;
	}

	function getData() {
		cardTitle = config.title;
		cardText = config.text;
		cardImportant = config.important;
	}

	function changeCard() {
		titleEl.style.display = 'none';
		textEl.style.display = 'none';

		changeTitleEl.style.display = 'block';
		changeTextEl.style.display = 'block';

		changeTitleEl.value = cardTitle;
		changeTextEl.value = cardText;

		changeButtonEl.textContent = 'SAVE';
		changeButtonEl.setAttribute('data-action', 'save');
	}

	function saveCard() {
		titleEl.textContent = changeTitleEl.value;
		textEl.textContent = changeTextEl.value;

		cardTitle = changeTitleEl.value;
		cardText = changeTextEl.value;

		changeTitleEl.style.display = 'none';
		changeTextEl.style.display = 'none';

		titleEl.style.display = 'block';
		textEl.style.display = 'block';

		changeButtonEl.textContent = 'CHANGE';
		changeButtonEl.setAttribute('data-action', 'change');

		updateState();
	}

	function attachEvents() {
		deleteButtonEl.addEventListener('click', function (event) {
			event.preventDefault();

			cardsBlock.removeChild(cardBlock);

			delete serverData[id];
		});

		changeButtonEl.addEventListener('click', function (event) {
			event.preventDefault();
			var action = changeButtonEl.getAttribute('data-action');

			switch (action) {
				case 'change':
					changeCard();
					break;
				case 'save':
					saveCard();
					break;
			}
		});
	}

	function generateId() {
		id = new Date().getTime();
	}

	return {
		init: function () {
			cardsBlock = document.querySelector('#cardsBlock');

			getData();
			generateCard();
			attachEvents();
		},
	}
}
