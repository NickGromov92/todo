'use strict';

var serverData = {
	54353: {
		title: 'First',
		text: 'Lorel',
		important: true
	},
	54354: {
		title: 'First',
		text: 'Lorel',
		important: true
	},
	54355: {
		title: 'First',
		text: 'Lorel',
		important: true
	}
};

for(var key in serverData) {
	var cardObj = serverData[key];
	cardObj.id = key;

	var card = Card(cardObj);
	card.init();
}

var createButton = document.querySelector('#createButton');
createButton.addEventListener('click', function (event) {
	event.preventDefault();
	var createForm = document.querySelector('#createForm');

	var cardObj = {};

	var titleField = createForm.querySelector('#title');
	var textField = createForm.querySelector('#text');
	var importantField = createForm.querySelector('#important');

	cardObj.title = titleField.value;
	cardObj.text = textField.value;
	cardObj.important = importantField.checked;

	titleField.value = '';
	textField.value = '';
	importantField.checked = false;

	var card = Card(cardObj);
	card.init();
});
